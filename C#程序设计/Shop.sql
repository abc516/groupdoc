USE [master]
GO
/****** Object:  Database [Shop]    Script Date: 2014/12/1 9:11:44 ******/
CREATE DATABASE [Shop] ON  PRIMARY 
( NAME = N'Shop_Data', FILENAME = N'D:\DOC\教学\C#程序设计\教案\例程\DB\Shop_Data.MDF' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'Shop_Log', FILENAME = N'D:\DOC\教学\C#程序设计\教案\例程\DB\Shop_Log.LDF' , SIZE = 1024KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
GO
ALTER DATABASE [Shop] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Shop].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [Shop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Shop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Shop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Shop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Shop] SET ARITHABORT OFF 
GO
ALTER DATABASE [Shop] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Shop] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Shop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Shop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Shop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Shop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Shop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Shop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Shop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Shop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Shop] SET RECOVERY FULL 
GO
ALTER DATABASE [Shop] SET  MULTI_USER 
GO
ALTER DATABASE [Shop] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [Shop] SET DB_CHAINING OFF 
GO
USE [Shop]
GO
/****** Object:  Table [dbo].[Journalising]    Script Date: 2014/12/1 9:11:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Journalising](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[OccurTime] [datetime] NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_Journalising] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KUser]    Script Date: 2014/12/1 9:11:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KUser](
	[ku_id] [int] IDENTITY(1,1) NOT NULL,
	[login_name] [varchar](60) NOT NULL,
	[password] [varchar](60) NOT NULL,
	[user_name] [varchar](60) NOT NULL,
	[mobile] [varchar](20) NULL,
	[email] [varchar](60) NULL,
	[status] [varchar](10) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[KUser] ON 

INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (2, N'sys', N'sys', N'Liang Yuen', N'133', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (3, N'姜维', N'120311120421', N'姜维', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (4, N'金飘飘', N'130311130101', N'金飘飘', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (5, N'张淼', N'130311130105', N'张淼', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (6, N'陈园园', N'130311130112', N'陈园园', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (7, N'樊雨婷', N'130311130202', N'樊雨婷', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (8, N'姚洁', N'130311130203', N'姚洁', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (9, N'袁金芳', N'130311130204', N'袁金芳', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (10, N'沈青文', N'130311130207', N'沈青文', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (11, N'朱芬芬', N'130311130208', N'朱芬芬', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (12, N'周浩燕', N'130311130210', N'周浩燕', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (13, N'王珍珠', N'130311130212', N'王珍珠', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (14, N'童弘', N'130311130214', N'童弘', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (15, N'陆单单', N'130311130215', N'陆单单', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (16, N'孟佳伟', N'130311130226', N'孟佳伟', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (17, N'金翔栋', N'130311130232', N'金翔栋', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (18, N'杨浩靓', N'130311130239', N'杨浩靓', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (19, N'韩军明', N'130311130245', N'韩军明', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (20, N'郑文豪', N'130311130246', N'郑文豪', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (21, N'汪圣奇', N'130311130247', N'汪圣奇', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (22, N'周煌珂', N'130311130249', N'周煌珂', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (23, N'王婷', N'130311130301', N'王婷', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (24, N'胡华媚', N'130311130304', N'胡华媚', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (25, N'徐金芳', N'130311130305', N'徐金芳', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (26, N'胡佳丽', N'130311130308', N'胡佳丽', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (27, N'周佳佳', N'130311130311', N'周佳佳', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (28, N'单少华', N'130311130314', N'单少华', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (29, N'钱莉莉', N'130311130315', N'钱莉莉', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (30, N'袁传民', N'130311130318', N'袁传民', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (31, N'孔佳峰', N'130311130330', N'孔佳峰', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (32, N'陈荣杰', N'130311130345', N'陈荣杰', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (33, N'孙嘉磊', N'130311130346', N'孙嘉磊', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (34, N'陈露丹', N'130311130406', N'陈露丹', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (35, N'陈莹', N'130311130407', N'陈莹', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (36, N'蒋婉儿', N'130311130408', N'蒋婉儿', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (37, N'姜丽艳', N'130311130412', N'姜丽艳', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (38, N'顾壮壮', N'130311130420', N'顾壮壮', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (39, N'傅俊铭', N'130311130430', N'傅俊铭', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (40, N'余力', N'130311130439', N'余力', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (41, N'徐帆', N'130311130441', N'徐帆', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (42, N'姚赛婵', N'130311130502', N'姚赛婵', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (43, N'李科婷', N'130311130505', N'李科婷', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (44, N'施晓丽', N'130311130510', N'施晓丽', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (45, N'沈洁婷', N'130311130512', N'沈洁婷', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (46, N'陆屹帆', N'130311130518', N'陆屹帆', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (47, N'缪烨鸣', N'130311130526', N'缪烨鸣', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (48, N'陈泓豪', N'130311130532', N'陈泓豪', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (49, N'曹继成', N'130311130541', N'曹继成', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (50, N'程泽垚', N'130311130546', N'程泽垚', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (51, N'周方俊', N'130311130547', N'周方俊', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (52, N'徐杰', N'110309110235', N'徐杰', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (53, N'邵文凯', N'120311120533', N'邵文凯', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (54, N'王炜', N'130311130102', N'王炜', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (55, N'李倩', N'130311130104', N'李倩', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (56, N'何思思', N'130311130107', N'何思思', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (57, N'谢莎妮', N'130311130108', N'谢莎妮', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (58, N'陈彩香', N'130311130110', N'陈彩香', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (59, N'吴琼', N'130311130111', N'吴琼', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (60, N'陈佳兰', N'130311130113', N'陈佳兰', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (61, N'徐晶', N'130311130114', N'徐晶', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (62, N'黄玲玲', N'130311130116', N'黄玲玲', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (63, N'赵晓宇', N'130311130118', N'赵晓宇', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (64, N'任淑婷', N'130311130120', N'任淑婷', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (65, N'马超', N'130311130126', N'马超', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (66, N'王波', N'130311130128', N'王波', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (67, N'柴炎华', N'130311130129', N'柴炎华', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (68, N'蔡振辉', N'130311130133', N'蔡振辉', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (69, N'唐浩南', N'130311130134', N'唐浩南', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (70, N'胡文杰', N'130311130135', N'胡文杰', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (71, N'杨鹏程', N'130311130137', N'杨鹏程', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (72, N'帅祺坤', N'130311130142', N'帅祺坤', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (73, N'王帅', N'130311130143', N'王帅', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (74, N'方钰哲', N'130311130144', N'方钰哲', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (75, N'杨少奇', N'130311130146', N'杨少奇', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (76, N'叶周航', N'130311130147', N'叶周航', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (77, N'戴佳惠', N'130311130216', N'戴佳惠', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (78, N'方克客', N'130311130229', N'方克客', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (79, N'林初活', N'130311130231', N'林初活', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (80, N'许从越', N'130311130250', N'许从越', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (81, N'叶佳红', N'130311130302', N'叶佳红', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (82, N'鄢安琴', N'130311130306', N'鄢安琴', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (83, N'单丽谕', N'130311130313', N'单丽谕', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (84, N'陈映映', N'130311130316', N'陈映映', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (85, N'陈犟', N'130311130350', N'陈犟', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (86, N'张芝洋', N'130311130415', N'张芝洋', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (87, N'李红波', N'130311130417', N'李红波', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (88, N'包剑能', N'130311130419', N'包剑能', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (89, N'俞锋炯', N'130311130423', N'俞锋炯', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (90, N'陈张松', N'130311130425', N'陈张松', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (91, N'陈凯', N'130311130428', N'陈凯', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (92, N'陈昌盛', N'130311130437', N'陈昌盛', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (93, N'蒋芳芳', N'130311130604', N'蒋芳芳', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (94, N'傅露露', N'130311130605', N'傅露露', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (95, N'徐康娜', N'130311130607', N'徐康娜', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (96, N'彭星月', N'130311130609', N'彭星月', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (97, N'章华孟', N'130311130618', N'章华孟', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (98, N'夏伟国', N'130311130639', N'夏伟国', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (99, N'马松祥', N'130311130645', N'马松祥', N'189', N'123@sina.com', N'激活')
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (100, N'陈志超', N'130311130647', N'陈志超', N'189', N'123@sina.com', N'激活')
GO
INSERT [dbo].[KUser] ([ku_id], [login_name], [password], [user_name], [mobile], [email], [status]) VALUES (101, N'任振坤', N'130311130649', N'任振坤', N'189', N'123@sina.com', N'激活')
SET IDENTITY_INSERT [dbo].[KUser] OFF
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日志流水ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Journalising', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录用户名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Journalising', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发生时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Journalising', @level2type=N'COLUMN',@level2name=N'OccurTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作类型：登录或注销' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Journalising', @level2type=N'COLUMN',@level2name=N'Type'
GO
USE [master]
GO
ALTER DATABASE [Shop] SET  READ_WRITE 
GO
